#include "menu.h"

namespace menu
{
	bool cambioDeEsena;
	namespace botonesName
	{
		botones* botonGeneral[static_cast<int>(salir) + 1];
		botones* botonOpciones[static_cast<int>(menos3) + 1];
		botones* botonAux;
	}
	namespace fondos
	{
		//intro.
		Texture2D intro;
		Texture2D devel;
		//general.
		Texture2D general;
		//Opciones.
		Texture2D opciones;
		//Creditos.
		Texture2D creditos;
		//inst.
		Texture2D instrucciones;
		//musica.
		Music musica;

	}
	namespace scenes
	{
		menu_scenes actual_menu_scenes;
		menu_scenes actual_menu_scenes_nueva;
	}

	void init()
	{
		
		using namespace scenes;
		switch (actual_menu_scenes)
		{
		case menu::scenes::menu_scenes::intro:
			fondos::musica = LoadMusicStream("res/assets/MusicaMenu.mp3");
			fondos::intro = LoadTexture("res/assets/titulo.png");
			fondos::devel = LoadTexture("res/assets/develop.png");
			break;
		case menu::scenes::menu_scenes::general:
			fondos::general = LoadTexture("res/assets/general.png");
			botonesName::botonGeneral[botonesName::jugar] = new botones(LoadTexture("res/assets/BotonJugar.png"), GetScreenWidth() / 2, GetScreenHeight() / 10 * 2);
			botonesName::botonGeneral[botonesName::opciones] = new botones(LoadTexture("res/assets/BotonOpciones.png"), GetScreenWidth() / 2, GetScreenHeight() / 10 * 4);
			botonesName::botonGeneral[botonesName::creditos] = new botones(LoadTexture("res/assets/BotonCreditos.png"), GetScreenWidth() / 2, GetScreenHeight() / 10 * 6);
			botonesName::botonGeneral[botonesName::salir] = new botones(LoadTexture("res/assets/BotonSalir.png"), GetScreenWidth() / 2, GetScreenHeight() / 10 * 8);
			break;
		case menu::scenes::menu_scenes::opciones:
			fondos::opciones = LoadTexture("res/assets/opciones.png");
			botonesName::botonOpciones[botonesName::atras] = new botones(LoadTexture("res/assets/BotonAtras.png"), 0 + GetScreenWidth() / 10*2,  GetScreenHeight() / 10 * 9);
			botonesName::botonOpciones[botonesName::mas] = new botones(LoadTexture("res/assets/BotonMas.png"), GetScreenWidth() / 10*4, GetScreenHeight() / 7 * 2);
			botonesName::botonOpciones[botonesName::menos] = new botones(LoadTexture("res/assets/BotonMenos.png"), GetScreenWidth() / 10 * 2, GetScreenHeight() / 7 * 2);
			botonesName::botonOpciones[botonesName::mas2] = new botones(LoadTexture("res/assets/BotonMas.png"), GetScreenWidth() / 10 * 9, GetScreenHeight() / 7 * 2);
			botonesName::botonOpciones[botonesName::menos2] = new botones(LoadTexture("res/assets/BotonMenos.png"), GetScreenWidth() / 10 * 7, GetScreenHeight() / 7 * 2);
			botonesName::botonOpciones[botonesName::mas3] = new botones(LoadTexture("res/assets/BotonMas.png"), GetScreenWidth() / 10 * 6.5f, GetScreenHeight() / 7 *5);
			botonesName::botonOpciones[botonesName::menos3] = new botones(LoadTexture("res/assets/BotonMenos.png"), GetScreenWidth() / 10 * 4, GetScreenHeight() / 7 * 5);
			botonesName::botonAux = new botones(LoadTexture("res/assets/BotonAceptar.png"), GetScreenWidth()/2, GetScreenHeight() / 10 * 9);


			break;
		case menu::scenes::menu_scenes::creditos:
			fondos::creditos = LoadTexture("res/assets/creditos.png");
			//creo que no se peude hacer esto. Tipo tengo que iniciar todos los botones.
			botonesName::botonAux = new botones(LoadTexture("res/assets/BotonAtras.png"), 0 + GetScreenWidth() / 10 * 2, GetScreenHeight() / 10 * 9);
			break;
		case menu::scenes::menu_scenes::instrucciones:
			fondos::instrucciones = LoadTexture("res/assets/instrucciones.png");
		}
	}
	void update()
	{
		UpdateMusicStream(fondos::musica);
		if (!IsMusicPlaying(fondos::musica))
		{
			PlayMusicStream(fondos::musica);
		}
		using namespace scenes;
		switch (actual_menu_scenes)
		{
		case menu::scenes::menu_scenes::intro:
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) )
			actual_menu_scenes_nueva = menu_scenes::general;
			break;
		case menu::scenes::menu_scenes::general:
			for (int i = 0; i <= botonesName::botonesGen::salir; i++)
			{
				if (botonesName::botonGeneral[i]->actualizarBoton())
				{
					switch (static_cast<botonesName::botonesGen>(i))
					{
					case botonesName::botonesGen::jugar:
						menu::scenes::actual_menu_scenes_nueva = menu::scenes::menu_scenes::instrucciones;
						break;
					case botonesName::botonesGen::opciones:
						menu::scenes::actual_menu_scenes_nueva = menu::scenes::menu_scenes::opciones;
						break;
					case botonesName::botonesGen::creditos:
						menu::scenes::actual_menu_scenes_nueva = menu::scenes::menu_scenes::creditos;
						break;
					case botonesName::botonesGen::salir:
						UnloadMusicStream(fondos::musica);
						endless::game::gameOver = true;
						break;
					}
				}
			}
			break;
		case menu::scenes::menu_scenes::opciones:
			for (int i = 0; i <= botonesName::botonesOp::menos3  ; i++)
			{
				if (botonesName::botonOpciones[i]->actualizarBoton())
				{
					switch (static_cast<botonesName::botonesOp>(i))
					{
					case botonesName::botonesOp::atras:
						menu::scenes::actual_menu_scenes_nueva = menu::scenes::menu_scenes::general;
						break;
					case botonesName::botonesOp::mas:
						//aumentar algo.
						if (endless::game::volumeFx<2)
						{
							endless::game::volumeFx += 0.1f;
						}
						break;
					case botonesName::botonesOp::menos:
						if (endless::game::volumeFx >= 0)
						{
							endless::game::volumeFx -= 0.1f;
						}
						//disminuye otra.
						break;
					case botonesName::botonesOp::mas2:
						//aumenta algo.
						if (endless::game::volumeMusic < 2)
						{
							endless::game::volumeMusic += 0.1f;
							SetMusicVolume(fondos::musica, endless::game::volumeMusic);
						}
						break;
					case botonesName::botonesOp::menos2:
						//disminuye otro.
						if (endless::game::volumeMusic > 0)
						{
							endless::game::volumeMusic -= 0.1f;
							SetMusicVolume(fondos::musica, endless::game::volumeMusic);
						}
						break;
					case botonesName::botonesOp::mas3:
						//disminuye otro.
						if (endless::game::resolucionSeleccionada<MAX_RESOLUSION)
						{
							endless::game::resolucionSeleccionada++;
						}
						break;
					case botonesName::botonesOp::menos3:
						//disminuye otro.
						if (endless::game::resolucionSeleccionada > 0)
						{
							endless::game::resolucionSeleccionada--;
						}
						break;
					}
					
				}
				if (botonesName::botonAux->actualizarBoton())
				{
					//actualizar ress.
					SetWindowSize(endless::game::resoluccion[endless::game::resolucionSeleccionada].x, endless::game::resoluccion[endless::game::resolucionSeleccionada].y);
					endless::game::resolucionCambio = true;
				}
				botonesName::botonOpciones[i]->cambiarVolumen();
				botonesName::botonAux->cambiarVolumen();
			}
			break;
		case menu::scenes::menu_scenes::creditos:
			if (botonesName::botonAux->actualizarBoton())
			menu::scenes::actual_menu_scenes_nueva = menu::scenes::menu_scenes::general;
			break;
		case menu::scenes::menu_scenes::instrucciones:
			if (IsKeyPressed(KEY_SPACE))
			{
				endless::game::newCurrentScene = endless::game::Scene::Game;
			}
		}
	}
	void draw()
	{
		using namespace scenes;
		switch (actual_menu_scenes)
		{
		case menu::scenes::menu_scenes::intro:
			DrawTextureRec(fondos::intro, { 0,0,static_cast<float>(fondos::intro.width),static_cast<float>(fondos::intro.height) }, { static_cast<float>(GetScreenWidth()) / 2 - fondos::intro.width / 2,static_cast<float>(GetScreenHeight()) / 2 - fondos::intro.height }, WHITE);
			DrawTextureRec(fondos::devel, { 0,0,static_cast<float>(fondos::devel.width),static_cast<float>(fondos::devel.height) }, { static_cast<float>(GetScreenWidth()) / 2 - fondos::devel.width / 2,static_cast<float>(GetScreenHeight()) / 2 - fondos::devel.height*2 }, WHITE);
			break;
		case menu::scenes::menu_scenes::general:
			fondos::general.width = GetScreenWidth();
			fondos::general.height = GetScreenHeight();
			DrawTextureRec(fondos::general, { 0,0,static_cast<float>(fondos::general.width),static_cast<float>(fondos::general.height) }, { 0,0 },WHITE);
			for (int i = 0; i <= botonesName::salir; i++)
			{

				botonesName::botonGeneral[i]->dibujarBoton();
#ifndef TEST
				botonesName::botonGeneral[i]->dibujarLineas();

#endif // !TESTING

			}
			break;
		case menu::scenes::menu_scenes::opciones:
			fondos::opciones.width = GetScreenWidth();
			fondos::opciones.height = GetScreenHeight();
			DrawTextureRec(fondos::opciones, { 0,0,static_cast<float>(fondos::opciones.width),static_cast<float>(fondos::opciones.height) }, { 0,0 }, WHITE);
			for (int i = 0; i <= botonesName::menos3 ; i++)
			{
				botonesName::botonOpciones[i]->dibujarBoton();
			}
			botonesName::botonAux->dibujarBoton();
			DrawText(FormatText("%i,%i",static_cast<int>(endless::game::resoluccion[endless::game::resolucionSeleccionada].x), static_cast<int>(endless::game::resoluccion[endless::game::resolucionSeleccionada].y)),GetScreenWidth()/2-MeasureText("000,0000",GetScreenHeight()/50)/2,GetScreenHeight()/10*7,GetScreenHeight()/50,GREEN);
			DrawText(FormatText("%f", (endless::game::volumeFx)), GetScreenWidth() / 4 + MeasureText("0000000", GetScreenHeight() / 50) / 2, GetScreenHeight() / 10 * 3, GetScreenHeight() / 50, GREEN);
			DrawText(FormatText("%f", (endless::game::volumeMusic)), GetScreenWidth() / 10*8 - MeasureText("0000000", GetScreenHeight() / 50) / 2, GetScreenHeight() / 10 * 3, GetScreenHeight() / 50, GREEN);
			DrawText("Musica", GetScreenWidth() / 10*8 - MeasureText("Musica", GetScreenHeight() / 20) / 2, GetScreenHeight() / 10 * 1, GetScreenHeight() / 20, GREEN);
			DrawText("Resolucion", GetScreenWidth() / 10 * 5 - MeasureText("Resolucion", GetScreenHeight() / 20) / 2, GetScreenHeight() / 10 * 6, GetScreenHeight() / 20, GREEN);

			DrawText("Efectos de sonido", GetScreenWidth() / 4 - MeasureText("Efectos de sonido", GetScreenHeight() / 20) / 2, GetScreenHeight() / 10 * 1, GetScreenHeight() / 20, GREEN);
			break;
		case menu::scenes::menu_scenes::creditos:
			fondos::creditos.width = GetScreenWidth();
			fondos::creditos.height = GetScreenHeight();
			DrawTextureRec(fondos::creditos, { 0,0,static_cast<float>(fondos::creditos.width),static_cast<float>(fondos::creditos.height) }, { 0,0 }, WHITE);
			botonesName::botonAux->dibujarBoton();
			break;
		case menu::scenes::menu_scenes::instrucciones:
			fondos::instrucciones.width = GetScreenWidth();
			fondos::instrucciones.height = GetScreenHeight();
			DrawTextureRec(fondos::instrucciones, { 0,0,static_cast<float>(fondos::instrucciones.width),static_cast<float>(fondos::instrucciones.height) }, { 0,0 }, WHITE);
		}
	}
	void deinit()
	{
		switch (scenes::actual_menu_scenes)
		{
		case menu::scenes::menu_scenes::intro:
			UnloadTexture(fondos::intro);
			UnloadTexture(fondos::devel);
			break;
		case menu::scenes::menu_scenes::general:
			UnloadTexture(fondos::general);
			for (int i = 0; i < botonesName::salir; i++)
			{
				delete botonesName::botonGeneral[i];
			}
			break;
		case menu::scenes::menu_scenes::opciones:
			UnloadTexture(fondos::opciones);
			for (int i = 0; i < botonesName::menos3 ; i++)
			{
				delete botonesName::botonOpciones[i];
			}
			delete botonesName::botonAux;
			break;
		case menu::scenes::menu_scenes::creditos:
			UnloadTexture(fondos::creditos);
			delete botonesName::botonAux;
			break;
		case menu::scenes::menu_scenes::instrucciones:
			UnloadTexture(fondos::instrucciones);
		}
	}
}
