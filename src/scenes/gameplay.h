#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include "raylib.h"
#include "controller/game.h"
#include "logica/Componentes.h"
//#include "logica/textura.h"

namespace gameplay
{
	extern bool pause;
	extern bool gameOver;
	extern Music musica;
	enum objetos
	{
		pocionMana,
		pocionVida,
		enemigoVolador,
		enemigoTerreste,
		paredDragon,
		oro
	};
	namespace mundo
	{
		const int cant_fondos = 12;
		const int cant_carriles = 3;
		const int cant_objetos = 6;
		extern fondos* fondo[cant_fondos];
		extern carriles* carril[cant_carriles];
		extern prop* objetos[cant_objetos];
	}
	namespace jugador
	{
		extern player* pj1;
	}
	extern void init();
	extern void update();
	extern void draw();
	extern void deinit();
}
#endif