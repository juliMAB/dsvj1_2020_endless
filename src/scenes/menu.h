#ifndef MENU_H
#define MENU_H
#include "raylib.h"
#include "controller/game.h"
#include "logica/componentesMenu.h"


namespace menu
{
	extern bool cambioDeEsena;
	namespace botonesName
	{
		enum botonesGen
		{
			jugar,
			opciones,
			creditos,
			salir

		};
		enum botonesOp
		{
			atras,
			mas,
			menos,
			mas2,
			menos2,
			mas3,
			menos3
		};
		extern botones* botonGeneral[static_cast<int>(salir) + 1];
		extern botones* botonOpciones[static_cast<int>(menos3) + 1];
		extern botones* botonAux;
	}
	namespace scenes
	{
		enum class menu_scenes
		{
			intro,
			general,
			opciones,
			instrucciones,
			creditos
		};
		extern menu_scenes actual_menu_scenes;
		extern menu_scenes actual_menu_scenes_nueva;
	}
	extern void init();
	extern void update();
	extern void draw();
	extern void deinit();
}
#endif