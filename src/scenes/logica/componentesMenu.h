#ifndef COMPONENTESMENU_H
#define COMPONENTESMENU_H
#include "raylib.h"
#include "controller/game.h"
class botones
{
public:
	botones(Texture2D textura, short x, short y);
	bool actualizarBoton();
	void dibujarBoton();
	void dibujarLineas();
	void cambiarVolumen();
	~botones();

private:
	short _estado;
	Rectangle _rec;
	short _altoFrame;
	Rectangle _imagenInterna;
	Texture2D _texture;
	Sound _sonido;
	bool _tocando;
};

#endif