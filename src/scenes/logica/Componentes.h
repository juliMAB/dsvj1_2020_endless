#ifndef COMPONENTES_H
#define COMPONENTES_H
#include "raylib.h"
#define CANT_ESTADOS 4
#define CANT_FRAMES 6


enum class playerAnimacion
{
	caminar,
	saltar,
	agachado,
	habilidad,
};
class player
{
public:
	player();
	//player(Vector2 pos, Vector2 vel, short magia, Texture2D textura, int oro, char SaltarTecla, bool activo);
	//acciones.
	void saltar(bool aire);
	void agacharse();
	void hablidad(); //DISPARAR, PU�ETASO.
	void cambiar_carril();
	void sumarOro();
	void sumarMana();
	void actualizarEstado(bool piso,bool accion);
	void actualizarFrame();
	void aplicarGravedad();
	void actualizarRecAnim();
	void actualizarRecPos();
	void actualizarPivotDePunta();
	void actualizarPivot();
	void resetVel();
	void corregirCollision(Rectangle piso);
	void setAire(bool aire);
	void dibujarOro();
	void dibujarMana();
	void dibujarVida();
	void restarVida();
	void sumarVida();
	void resetearPj();
	//devolver
	Rectangle darRec();
	Rectangle darSource();
	Texture2D darTetura();
	Vector2 darPivotDePunta();
	bool darActivo();
	short darCarril();
	bool darAire();
	short darEstado();
	Vector2 darVel();
	short darVida();
	short darOro();

	
	//destructor.
	~player();
	
private:
	Vector2 _pivot; //posicion pivot "el centro del rectangulo".
	Vector2 _pivotDePunta;
	Vector2 _vel; //velocidad.
	short _magia;	 //mana.
	Texture2D _textura;
	
	int _oro;	 //oro.
	bool _activo; //por ahora inutil.
	bool _aire;	 //estoy en el aire? //inutil.
	float _largoFrame;
	float _altoFrame;
	Rectangle _imagenInterna;
	Rectangle _rec;
	float _frameActual;
	short _estado;
	float _gravedad;
	float _resistencia;
	short _animTimer;
	short _carril;
	short _vida;
	//sonidos
	Sound _sSalto;
	Sound _sBajar;
	Sound _sGolpe;
	Sound _sMagia;
	Sound _sOro;
};
 
class fondos
{
public:
	//constructor.
	fondos();
	fondos(Rectangle pos,float vel, Texture2D textura);
	//acciones.
	void moverFondo();
	//debolucion.
	Texture2D darTextura();
	Rectangle darRec();
	Rectangle darImagenInternar();
	//destructor.
	~fondos();

private:
	float _vel;
	Texture2D _textura;
	Rectangle _rec;
	Rectangle _imagenInterna;
};
class carriles
{
public:
	carriles(int pos);
	Rectangle darPiso();
private:
	Rectangle _piso;
};

class prop
{
public:
	prop(bool agresivo,Texture2D textura, short tiempoRespawn); //construir.
	Rectangle darRec();
	void moverse();
	void spawnear(int velocidad,int pos);
	void dibujar();
	void cambiarActivo();
	void reiniciar();
	~prop();

private:
	bool _activo;
	bool _agresivo;
	Rectangle _rec;
	Rectangle _imagenInterna;
	Vector2 _vel;
	Texture2D _textura;
	short _tiempoRespawn;
	short _itiempoRespawn;
	
};




#endif