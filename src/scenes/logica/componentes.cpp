#include "Componentes.h"

player::player()
{
	_textura = LoadTexture("res/assets/Gost.png");
	_textura.height *= static_cast<int>(GetScreenHeight() / 180);  //modificarlo despues.
	_textura.width  *= static_cast<int>(GetScreenWidth()  / 320);  //modificarlo despues.
	_largoFrame = (static_cast<float>(_textura.width) / 6);		   //cantidad frames x estado.
	_altoFrame = (static_cast<float>(_textura.height) / 4);		   //cantidad de estados.
	_pivot = { _largoFrame, static_cast<float>(GetScreenHeight()/2) };
	_imagenInterna = { 0, 0, _largoFrame, _altoFrame };
	_rec = { _pivot.x - _largoFrame / 2,_pivot.y - _altoFrame / 2,_largoFrame,_altoFrame };
	_pivotDePunta = { 0,0 };
	_frameActual = 0;
	_estado = 0;
	_gravedad = (static_cast<float>(GetScreenHeight()) / 156);
	_vel = { 0,0 };
	_resistencia = 0;
	_animTimer = 0;
	_activo = true;
	_carril = 3;
	_oro = 0;
	_magia = 2;
	_vida = 3;
	_aire = true;
	_sSalto = LoadSound("res/assets/salto.wav");
	_sBajar = LoadSound("res/assets/bajar.wav");
	_sGolpe = LoadSound("res/assets/golpe.wav");
	_sMagia = LoadSound("res/assets/magia.wav");
	_sOro = LoadSound("res/assets/oro.wav");

}

void player::saltar(bool piso)
{
	if (piso)
	{
		//salto
		_vel = { 0,-static_cast<float>(GetScreenHeight()) /156 };
		_vel.y /= 1.249999f;
		PlaySoundMulti(_sSalto);
		
	}
	else
	{
		
		//planeo.
		if (_vel.y>0)
		{
			_vel.y = static_cast<float>(GetScreenHeight())/15.6f * GetFrameTime();
		}
		
	}
	
}

void player::agacharse()
{
	_estado = 2;
	_animTimer=30;
	if (_aire)
	{
		if (_rec.y+_rec.height<= (GetScreenHeight() / 3) * _carril)
		{
			_vel.y = GetScreenHeight()/26;
			PlaySoundMulti(_sBajar);
		}
		
	}
}

void player::hablidad()
{
	if (_magia>0 && _estado !=3)
	{
		_estado = 3;
		_animTimer = 30;
		_magia--;
		PlaySoundMulti(_sMagia);
	}
	
}

void player::cambiar_carril()
{
	if (IsKeyPressed(KEY_UP))
	{
		if (_carril != 1)
		{
			_carril--;
			_vel = { 0,-static_cast<float>(GetScreenHeight()) / 156};
			PlaySoundMulti(_sSalto);
		}
	}
	
	if (IsKeyPressed(KEY_DOWN))
		if (_carril != 3)
		{
			_carril++;
			_vel = { 0,(static_cast<float>(GetScreenHeight()) / 3 / 1.2f) * GetFrameTime() };
			PlaySoundMulti(_sBajar);
		}
		
}

void player::sumarOro()
{
	_oro++;
	PlaySoundMulti(_sOro);
}

void player::sumarMana()
{
	_magia++;
}

player::~player()
{
	UnloadTexture(_textura);
	UnloadSound(_sSalto);
	UnloadSound(_sGolpe);
	UnloadSound(_sMagia);
	UnloadSound(_sOro);
	UnloadSound(_sBajar);
}

void player::actualizarEstado(bool piso,bool accion)
{
	if (!accion)
	{
		if (piso)
		{
			if (_animTimer <= 0)
			{
				_estado = 0;
			}
			else
			{
				_animTimer--;
			}

		}
		else if (!piso)
		{
			if (_animTimer <= 0)
			{
				_estado = 1;
			}
			else
			{
				_animTimer--;
			}
		}
	}
}

void player::actualizarFrame()
{
	//la velocidad va a tener que depender de el alto, asi que esto se va a tener que correguir.
	
	switch (static_cast<int>(_estado))
	{
	case 0: //estoy caminando.
		_frameActual++;
		if (_frameActual >= CANT_FRAMES)
		{
			_frameActual = 0;
		}
		break;
	case 1: //estoy saltando.
		if (_vel.y > 2 && _vel.y > 0)
		{
			_frameActual = 1;
		}
		else if (_vel.y < 2 && _vel.y > 0)
		{
			_frameActual = 2;
		}
		else if (_vel.y > -2 && _vel.y < 0)
		{
			_frameActual = 3;
		}
		else
		{
			_frameActual = 4;
		}
		break;
	case 2: //estoy agachando.
		if (!_aire)
		{
			if (_animTimer < 30 && _animTimer >25)
				_frameActual = 1;
			else if (_animTimer < 25 && _animTimer >5)
				if (_animTimer % 2 == 0)
					_frameActual = 2;
				else
					_frameActual = 3;
			else if (_animTimer < 5 && _animTimer >0)
				_frameActual = 4;
		}
		else
		{
			_estado = 1;
			_animTimer = 0;
		}
		
		break;
	case 3: //estoy magia.
		if (_animTimer < 30 && _animTimer >28)
			_frameActual = 1;
		else if (_animTimer < 28 && _animTimer >5)
			if (_animTimer % 2 == 0)
				_frameActual = 2;
			else
				_frameActual = 3;
		else if (_animTimer < 5 && _animTimer >0)
			_frameActual = 5;
		break;
	default:
		break;
	}
}

void player::aplicarGravedad()
{
	_gravedad;
	_vel.y += (_gravedad*GetFrameTime());
}

void player::actualizarRecAnim()
{
	_imagenInterna = { _largoFrame * _frameActual ,_altoFrame * _estado,_imagenInterna.width,_imagenInterna.height };
}

void player::actualizarRecPos()
{
	_rec =  { _pivot.x - _largoFrame / 2,_pivot.y - _altoFrame / 2,_rec.width,_rec.height };
}

void player::actualizarPivotDePunta()
{
	_pivotDePunta = { _pivot.x - _largoFrame / 2, _pivot.y - _altoFrame / 2 };

}

void player::actualizarPivot()
{
	_pivot = { _pivot.x + _vel.x,_pivot.y + _vel.y };
}

void player::resetVel()
{
	if (_vel.y !=0)
	{
		_vel.y = 0;
	}
}

void player::corregirCollision(Rectangle piso)
{
	if (_pivot.y + _rec.height > piso.y + 2) //si me paso por 2 pixeles debaja del piso
	{
		_pivot.y -= ((_pivot.y + _rec.height/2) - piso.y)-1;
	}
	
}

void player::setAire(bool aire)
{
	_aire = aire;
}

void player::dibujarOro()
{
	DrawText(FormatText("ORO: %i", _oro), 0, 0, GetScreenHeight() / 60, RED);
}

void player::dibujarMana()
{
	DrawText(FormatText("MANA: %i", _magia), 0, GetScreenHeight() / 60, GetScreenHeight() / 60, RED);
}

void player::dibujarVida()
{
	DrawText(FormatText("VIDA: %i", _vida), 0, GetScreenHeight() / 60*2, GetScreenHeight() / 60, RED);
}

void player::restarVida()
{
	_vida--;
	PlaySound(_sGolpe);
}

void player::sumarVida()
{
	_vida++;
}

void player::resetearPj()
{
	_rec = { 0, 0 ,_rec.width,_rec.height };
	_carril = 3;
	_vel = { 0,0 };
	_frameActual = 0;
	_estado = 0;
	_gravedad = GetScreenHeight()/156;
	_vel = { 0,0 };
	_resistencia = 0;
	_animTimer = 0;
	_activo = true;
	_carril = 3;
	_oro = 0;
	_magia = 2;
	_vida = 3;
	_aire = true;
}

Rectangle player::darRec()
{
	return _rec;
}

Rectangle player::darSource()
{
	return _imagenInterna;
}

Texture2D player::darTetura()
{
	return _textura;
}

Vector2 player::darPivotDePunta()
{
	return _pivotDePunta;
}

bool player::darActivo()
{
	return _activo;
}

short player::darCarril()
{
	return _carril;
}

bool player::darAire()
{
	return _aire;
}

short player::darEstado()
{
	return _estado;
}

Vector2 player::darVel()
{
	return _vel;
}

short player::darVida()
{
	return _vida;
}

short player::darOro()
{
	return _oro;
}

fondos::fondos()
{
	_vel = 1;
	_textura = Texture2D();
	_imagenInterna = {1,1,1,1};
	_rec = { 0,0,0,0 };

}

fondos::fondos(Rectangle rec, float vel, Texture2D textura)
{
	_vel = vel;
	_textura = textura;
	_textura.height = static_cast<int>(GetScreenHeight());  //modificarlo despues.
	_textura.width = static_cast<int>(GetScreenWidth());  //modificarlo despues.
	_imagenInterna = { 0, 0, static_cast<float>(_textura.width), static_cast<float>(_textura.height) };
	_rec = { rec.x,rec.y,static_cast<float>(_textura.width),static_cast<float>(_textura.height) };
}

void fondos::moverFondo()
{
	_imagenInterna.x+=_vel; //mover fondo dependiendo de la vel.

	//reiniciar fondo.
	if (_imagenInterna.x+_rec.width<=0)
	{
		_imagenInterna.x = static_cast<float>(GetScreenWidth());
	}
}

Texture2D fondos::darTextura()
{
	return _textura;
}

Rectangle fondos::darRec()
{
	return _rec;
}

Rectangle fondos::darImagenInternar()
{
	return _imagenInterna;
}

fondos::~fondos()
{
	UnloadTexture(_textura);
}

carriles::carriles(int pos)
{
	float altoCarril = (static_cast<float>(GetScreenHeight()) / 3);
	float altoSuelo = altoCarril / 3;
	_piso = { 0,altoSuelo * 3 * pos-1,static_cast<float>(GetScreenWidth()),1 };
}

Rectangle carriles::darPiso()
{
	return _piso;
}

prop::prop(bool agresivo, Texture2D textura, short tiempoRespawn)
{
	_agresivo = agresivo;
	_textura = textura;
	_textura.height *= static_cast<float>(GetScreenHeight())/ 3120;
	_textura.width *= static_cast<float>(GetScreenWidth()) / 5120;
	_rec = { -static_cast<float>(_textura.width) ,0,static_cast<float>(_textura.width),static_cast<float>(_textura.height) };
	_imagenInterna = { 0, 0, (static_cast<float>(_textura.width)) , (static_cast<float>(_textura.height)) };
	_vel = { 0,0 };
	_tiempoRespawn = (GetRandomValue(100, 1000));
	_itiempoRespawn = tiempoRespawn;
	_activo = true;
}

Rectangle prop::darRec()
{
	if (_activo)
	{
		return _rec;
	}
	else
	{
		return {0,0,0,0};
	}
	
}

void prop::moverse()
{
	if (_rec.x+_rec.width>0)
	{
		_rec.x -= _vel.x;
		_tiempoRespawn = _itiempoRespawn;
	}
	
}

void prop::spawnear(int velocidad, int obj)
{
	if (_rec.x<0)
	{
		_tiempoRespawn--;
	}
	if (_rec.x+_rec.width <= 0 && _tiempoRespawn <= 0) // ya paso al otro lado.
	{
		int puntoDeSpawn = 0;
		int pos = 0;
		while (puntoDeSpawn ==  0 || puntoDeSpawn ==  4 || puntoDeSpawn ==  8 || puntoDeSpawn ==  12 || puntoDeSpawn == 1|| puntoDeSpawn == 5|| puntoDeSpawn == 9)
		{
			puntoDeSpawn = GetRandomValue(1, 12);
			pos = GetScreenHeight() / 3 / 4 * puntoDeSpawn;
			if (obj == 4)
			{
				if (puntoDeSpawn == 1 || puntoDeSpawn == 3 || puntoDeSpawn == 5 || puntoDeSpawn == 7 || puntoDeSpawn == 9 || puntoDeSpawn == 11 || puntoDeSpawn == 13)
				{
					puntoDeSpawn = 0;
				}	
			}

		}
		_rec = { static_cast<float>(GetScreenWidth()),static_cast<float>(pos)-_rec.height/2,_rec.width,_rec.height }; // lo mando al borde y la pos en altura.
		_vel.x = static_cast<float>(velocidad * GetScreenWidth() / 1280);
		_activo = true;
	}
	
	
}

void prop::dibujar()
{
	if (_activo)
	{
		DrawTextureRec(_textura, _imagenInterna, { _rec.x, _rec.y }, WHITE);
	}
#ifdef TESTING
	DrawRectangleLines(_rec.x, _rec.y, _rec.width, _rec.height, GREEN);
#endif // TEST
}

void prop::cambiarActivo()
{
	_activo = !_activo;
}

void prop::reiniciar()
{
	_rec = { -static_cast<float>(_textura.width) ,0,static_cast<float>(_textura.width),static_cast<float>(_textura.height) };
	_imagenInterna = { 0, 0, (static_cast<float>(_textura.width)) , (static_cast<float>(_textura.height)) };
	_vel = { 0,0 };
	_activo = false;
}

prop::~prop()
{
	UnloadTexture(_textura);
}


