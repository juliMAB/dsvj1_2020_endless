#include "componentesMenu.h"

botones::botones(Texture2D textura,short x, short y)
{
	_texture = textura;
	_texture.height *= static_cast<float>(GetScreenHeight())/1560;
	_texture.width *= static_cast < float>(GetScreenWidth())/ 2560;
	/*_texture.height = (GetScreenHeight() / 10*2);
	_texture.width = (GetScreenWidth() / 6*2);*/
	_altoFrame = _texture.height / 2;
	_rec = { static_cast<float>(x-_texture.width/2),static_cast<float>(y- _altoFrame /2),static_cast<float>(_texture.width),static_cast<float>(_altoFrame)};
	_imagenInterna = { 0,0,static_cast<float>(_texture.width),static_cast<float>(_texture.height/2) };
	_estado = 0;
	_sonido = LoadSound("res/assets/botonS.wav");
	_tocando = false;
	SetSoundVolume(_sonido,endless::game::volumeFx);
}

bool botones::actualizarBoton()
{
	Vector2 mouse = GetMousePosition();
	if (CheckCollisionPointRec(mouse,_rec))
	{
		
		_estado = 1;
		if (!_tocando)
		{
			PlaySound(_sonido);
			_tocando = true;
		}
		
		
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			return true;
		}
		return false;
	}
	else
	{
		_tocando = false;
		_estado = 0;
		return false;
	}
	return false;
}

void botones::dibujarBoton()
{
	_imagenInterna = { 0,static_cast<float>(_altoFrame * _estado),static_cast<float>(_texture.width),static_cast<float>(_altoFrame) };
	DrawTextureRec(_texture, _imagenInterna, { _rec.x, _rec.y },WHITE);
}

void botones::dibujarLineas()
{
	DrawRectangleLines(_rec.x, _rec.y, _rec.width, _rec.height, WHITE);
}

void botones::cambiarVolumen()
{
	SetSoundVolume(_sonido,endless::game::volumeFx);
}

botones::~botones()
{
	UnloadSound(_sonido);
	UnloadTexture(_texture);
}
