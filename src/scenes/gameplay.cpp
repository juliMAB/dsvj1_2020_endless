#include "gameplay.h"
#include <iostream>
//0= fondo3;
namespace gameplay
{
	bool gameOver;
	bool pause;
	Music musica;
	short contador60;
	int tiempo;
	namespace mundo
	{
		
		fondos* fondo[cant_fondos];
		carriles* carril[cant_carriles];
		prop* objetos[cant_objetos];
	}
	namespace jugador
	{
		player* pj1;
		short anim_pj;
		short duracion_accion;
	}
	void init()
	{
		//timer.
		contador60 = 0;
		tiempo = 0;
		//jugador.
		jugador::pj1 = new player();
		jugador::anim_pj = 0;
		//fondos.
		mundo::fondo[0] = new fondos({ 0,0,0,0 }, 0.2f, LoadTexture("res/assets/fondo3.png"));
		mundo::fondo[1] = new fondos({ 0,0,0,0 }, 0.4f, LoadTexture("res/assets/fondo2.png"));
		mundo::fondo[2] = new fondos({ 0,0,0,0 }, 0.6f, LoadTexture("res/assets/fondo1.png"));
		mundo::fondo[3] = new fondos({ 0,0,0,0 }, 0.8f, LoadTexture("res/assets/arboles.png"));
		mundo::fondo[4] = new fondos({ 0,0,0,0 }, 1, LoadTexture("res/assets/piso.png"));
		mundo::fondo[5] = new fondos({ 0,0,0,0 }, 1.4f, LoadTexture("res/assets/frontal.png"));
		mundo::fondo[6] = new fondos({ static_cast<float>(GetScreenWidth()),0,0 }, 0.2f, LoadTexture("res/assets/fondo3.png"));
		mundo::fondo[7] = new fondos({ static_cast<float>(GetScreenWidth()),0,0 }, 0.4f, LoadTexture("res/assets/fondo2.png"));
		mundo::fondo[8] = new fondos({ static_cast<float>(GetScreenWidth()),0,0 }, 0.6f, LoadTexture("res/assets/fondo1.png"));
		mundo::fondo[9] = new fondos({ static_cast<float>(GetScreenWidth()),0,0 }, 0.8f, LoadTexture("res/assets/arboles.png"));
		mundo::fondo[10] = new fondos({ static_cast<float>(GetScreenWidth()),0,0 }, 1, LoadTexture("res/assets/piso.png"));
		mundo::fondo[11] = new fondos({ static_cast<float>(GetScreenWidth()),0,0 }, 1.4f, LoadTexture("res/assets/frontal.png"));

		//carriles.
		for (int i = 0; i < mundo::cant_carriles; i++)
		{
			mundo::carril[i] = new carriles(i + 1);
		}
		//objetos.

		mundo::objetos[pocionMana] = new prop(false, LoadTexture("res/assets/mana.png"), GetRandomValue(500, 600));
		mundo::objetos[pocionVida] = new prop(false, LoadTexture("res/assets/vida.png"), GetRandomValue(500, 600));
		mundo::objetos[enemigoVolador] = new prop(true, LoadTexture("res/assets/MonstruoVolador.png"), GetRandomValue(50, 100));
		mundo::objetos[enemigoTerreste] = new prop(true, LoadTexture("res/assets/MonstruoTerreste.png"), GetRandomValue(50, 100));
		mundo::objetos[paredDragon] = new prop(true, LoadTexture("res/assets/dragon.png"), GetRandomValue(250, 300));
		mundo::objetos[oro] = new prop(false, LoadTexture("res/assets/oro.png"), GetRandomValue(10, 50));
			
		
		musica = LoadMusicStream("res/assets/musicag.mp3");
	}
	void update()
	{
		using namespace jugador;
		
		if (!gameOver)
		{
			
			if (!pause)
			{
				//timer
				contador60++;
				if (contador60>=60)
				{
					contador60 = 0;
					tiempo++;
				}

				// musica
				UpdateMusicStream(musica);
				if (!IsMusicPlaying(musica))
				{
					PlayMusicStream(musica);
				}
				// gravedad
				if (jugador::pj1->darActivo())
				{
					if (CheckCollisionRecs(jugador::pj1->darRec(), mundo::carril[jugador::pj1->darCarril() - 1]->darPiso()))
					{

						if (static_cast<int>(jugador::pj1->darVel().y) >= 0)
						{

							pj1->corregirCollision(mundo::carril[jugador::pj1->darCarril() - 1]->darPiso());
							pj1->resetVel();
						}
						pj1->setAire(false);
					}
					else
					{


						pj1->setAire(true);

					}
					pj1->aplicarGravedad();
					pj1->actualizarPivot();
				}


				//inputs
#ifdef TESTING
				if (IsKeyPressed(KEY_W))
				{
					SetMusicVolume(musica, 0);
				}
#endif
				if (IsKeyDown(KEY_A))
				{
					pj1->saltar(CheckCollisionRecs(jugador::pj1->darRec(), mundo::carril[jugador::pj1->darCarril() - 1]->darPiso()));
				}
				if (IsKeyPressed(KEY_Z))
				{
					pj1->agacharse(); //estoy realizando la agachacion.
				}
				if (IsKeyPressed(KEY_X))
				{
					pj1->hablidad(); // estoy haciendo magia.
				}
				if (!pj1->darAire()) // solo se puede cambiar el carril si estoy en el piso.
				{
					pj1->cambiar_carril(); //cambiar carril, salto hacia arriba o cae con la gravedad.
				}
				if (IsKeyPressed(KEY_P))
					pause = !pause;
				//objetos
				for (int i = 0; i < mundo::cant_objetos; i++)
				{
					mundo::objetos[i]->spawnear(GetRandomValue(10, 15), i);
					mundo::objetos[i]->moverse();
				}

				//coliciones?
				for (int i = 0; i < mundo::cant_objetos; i++)
				{
					//si esta agachado.
					if (jugador::pj1->darEstado() == 2) //esta agachado.
					{
						if (CheckCollisionRecs({ jugador::pj1->darRec().x,jugador::pj1->darRec().y + jugador::pj1->darRec().height / 2,jugador::pj1->darRec().width,jugador::pj1->darRec().height / 2 }, mundo::objetos[i]->darRec()))
						{
							mundo::objetos[i]->cambiarActivo();
							if (i == pocionMana)
							{
								jugador::pj1->sumarMana();
							}
							else if (i == pocionVida)
							{
								jugador::pj1->sumarVida();
							}
							else if (i >= enemigoVolador && i <= paredDragon)
							{
								jugador::pj1->restarVida();
							}
							else if (i == oro)
							{
								jugador::pj1->sumarOro();
							}


						}
					}
					//no esta agachado.
					else
					{
						//esta haciendo magia.
						if (jugador::pj1->darEstado() == 3) //haciendo magia
						{
							if (CheckCollisionRecs(jugador::pj1->darRec(), mundo::objetos[i]->darRec()))
							{
								mundo::objetos[i]->cambiarActivo();
								if (i == pocionMana)
								{
									jugador::pj1->sumarMana();
								}
								if (i == pocionVida)
								{
									jugador::pj1->sumarVida();
								}
								if (i == paredDragon)
								{
									jugador::pj1->restarVida();
								}
								else if (i == oro)
								{
									jugador::pj1->sumarOro();
								}

							}
						}
						else
						{
							if (CheckCollisionRecs(jugador::pj1->darRec(), mundo::objetos[i]->darRec()))
							{
								mundo::objetos[i]->cambiarActivo();
								if (i == pocionMana)
								{
									jugador::pj1->sumarMana();
								}
								if (i == pocionVida)
								{
									jugador::pj1->sumarVida();
								}
								if (i >= enemigoVolador && i <= paredDragon)
								{
									jugador::pj1->restarVida();
								}
								else if (i == oro)
								{
									jugador::pj1->sumarOro();
								}

							}
						}

					}

				}


				//actualizar anim timers
				if (jugador::anim_pj <= 0)
				{
					jugador::pj1->actualizarFrame();
					jugador::anim_pj = 5;
					jugador::pj1->actualizarRecAnim();
					jugador::pj1->actualizarEstado(CheckCollisionRecs(jugador::pj1->darRec(), mundo::carril[jugador::pj1->darCarril() - 1]->darPiso()), duracion_accion != 0);
				}
				else
				{
					jugador::anim_pj--;
				}
				for (int i = 0; i < mundo::cant_fondos; i++)
				{
					mundo::fondo[i]->moverFondo();
				}



				//como voy a trabajar con el centro de del rectangulo player, voy a tener que pasarale los valores a el _rec que lo ocupa.
				pj1->actualizarPivot();
				pj1->actualizarRecPos();
				pj1->actualizarPivotDePunta();

				//confirmacion de derrota.
				if (jugador::pj1->darVida() <= 0)
				{
					gameOver = true;
				}
			}
			else
			{
				if (IsKeyPressed(KEY_P))
					pause = !pause;
				if (IsKeyPressed(KEY_G))
				{
					endless::game::newCurrentScene = endless::game::Scene::Menu;
					pj1->resetearPj();
					for (int i = 0; i < mundo::cant_objetos; i++)
					{
						mundo::objetos[i]->reiniciar();
					}
					gameOver = false;
				}
			}
			
		}
		else
		{
			if (IsKeyPressed(KEY_R))
			{
				pj1->resetearPj();
				for (int i = 0; i < mundo::cant_objetos; i++)
				{
					mundo::objetos[i]->reiniciar();
				}
				gameOver = false;
			}
			if (IsKeyPressed(KEY_G))
			{
				endless::game::newCurrentScene = endless::game::Scene::Menu;
				pj1->resetearPj();
				for (int i = 0; i < mundo::cant_objetos; i++)
				{
					mundo::objetos[i]->reiniciar();
				}
				gameOver = false;
			}
		}
	}
	void draw()
	{
		//dibujar fondos.
		for (int i = 0; i < mundo::cant_fondos-1; i++)
		{
			DrawTextureRec(mundo::fondo[i]->darTextura(), mundo::fondo[i]->darImagenInternar(), { mundo::fondo[i]->darRec().x,mundo::fondo[i]->darRec().y }, WHITE);
		}
		DrawTextureRec(jugador::pj1->darTetura(), jugador::pj1->darSource(), jugador::pj1->darPivotDePunta(), WHITE);
		//dibujar fondo frontal
		DrawTextureRec(mundo::fondo[5]->darTextura(), mundo::fondo[5]->darImagenInternar(), { mundo::fondo[5]->darRec().x,mundo::fondo[5]->darRec().y }, WHITE);
#ifdef TESTING
		DrawRectangleLines(static_cast<int>(jugador::pj1->darRec().x), static_cast<int>(jugador::pj1->darRec().y), static_cast<int>(jugador::pj1->darRec().width), static_cast<int>(jugador::pj1->darRec().height), GREEN);
		//linear referencia pisos.
		for (int i = 0; i < mundo::cant_carriles; i++)
		{
			DrawRectangleLines(mundo::carril[i]->darPiso().x, mundo::carril[i]->darPiso().y, mundo::carril[i]->darPiso().width, mundo::carril[i]->darPiso().height, WHITE);
		}
#endif // TESTING

		
		jugador::pj1->dibujarOro();
		jugador::pj1->dibujarMana();
		jugador::pj1->dibujarVida();
		DrawText(FormatText("Tiempo: %i",tiempo),0, GetScreenHeight() / 60 * 3, GetScreenHeight() / 60, RED);
#ifdef TESTING
		static float ALTO;
		if (IsKeyPressed(KEY_H))
		{
			ALTO = 0;
		}
		if (jugador::pj1->darVel().y < ALTO)
		{
			ALTO = jugador::pj1->darVel().y;
		}
		DrawText(FormatText("MAXIMO SALTO: %f", ALTO), GetScreenWidth() / 2 - MeasureText("ORO GANADO: XX", GetScreenHeight() / 50) / 2, GetScreenHeight() / 2 - GetScreenHeight() / 50 * 2, GetScreenHeight() / 50, RED);

#endif // TESTING

		
		//dibujar objto
		for (int i = 0; i < mundo::cant_objetos; i++)
		{
			mundo::objetos[i]->dibujar();
		}
		if (gameOver)
		{
			static unsigned char x;
			if (x!=255)
			{
				if (x < 0 || x>255)
				{
					x = 0;
				}
				x += 5;
			}
			DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), { 0,0,0,x });
			
			if (x == 255)
			{
				DrawText(FormatText("FIN DEL JUEGO"), GetScreenWidth() / 2 - MeasureText("FIN DEL JUEGO", GetScreenHeight() / 50) / 2, GetScreenHeight() / 2, GetScreenHeight() / 50, RED);
				DrawText(FormatText("ORO GANADO: %i",jugador::pj1->darOro()), GetScreenWidth() / 2 - MeasureText("ORO GANADO: XX", GetScreenHeight() / 50) / 2, GetScreenHeight() / 2- GetScreenHeight() / 50*2, GetScreenHeight() / 50, RED);
				DrawText(FormatText("TIEMPO JUGADO: %i ", tiempo), GetScreenWidth() / 2 - MeasureText("TIEMPO JUGADO: %i", GetScreenHeight() / 50) / 2, GetScreenHeight() / 2 - GetScreenHeight() / 50 * 5, GetScreenHeight() / 50, RED);
				DrawText(FormatText("[G] MENU , [R] RESETEAR PARTIDA"), GetScreenWidth() / 2 - MeasureText("[G] MENU , [R] RESETEAR PARTIDA", GetScreenHeight() / 50) / 2, GetScreenHeight() / 2 - GetScreenHeight() / 50*4, GetScreenHeight() / 50, RED);
			}
		}
		else
		{
			if (pause)
			{
				DrawText(FormatText("PAUSA"), GetScreenWidth() / 2 - MeasureText("PAUSA", GetScreenHeight() / 30) / 2, GetScreenHeight() / 2, GetScreenHeight() / 30, GREEN);
				DrawText(FormatText("APRETA [G] para ir a general"), GetScreenWidth() / 2 - MeasureText("APRETA [G] para ir a general", GetScreenHeight() / 30) / 2, GetScreenHeight() / 10*6, GetScreenHeight() / 30, GREEN);
			}
			
		}

		
	}
	void deinit()
	{
		
		delete jugador::pj1;
		for (int i = 0; i < mundo::cant_fondos; i++)
		{
			
			delete mundo::fondo[i];
		}
		for (int i = 0; i < mundo::cant_objetos; i++)
		{
			
			delete mundo::objetos[i];
		}
		UnloadMusicStream(musica);
	}
}