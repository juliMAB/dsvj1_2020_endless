#include "game.h"
#include "scenes/menu.h"
#include "scenes/gameplay.h"
#include "raylib.h"

namespace endless
{
	namespace game
	{

		Scene currentScene;
		Scene newCurrentScene;
		bool gameOver;
		float volumeFx;
		float volumeMusic;
		bool resolucionCambio;
		short resolucionSeleccionada = 5;
		Vector2 resoluccion[7] =
		{
			//0 //4:3
			{320,240},
			//1
			{512,384},
			//2
			{640,480},
			//3
			{800,600},
			//4
			{1024,768},
			//5 //16:9
			{1280,720},
			//6 
			{1920,1080}
		};

		
		void run()
		{
			init();

			while (!gameOver)
			{
				update();
				draw();
			}
			deinit();
			if (gameOver)  WindowShouldClose();

		}

		static void init()
		{

			InitWindow(static_cast<int>(resoluccion[resolucionSeleccionada].x), static_cast<int>(resoluccion[resolucionSeleccionada].y),"ENDLESS");
			SetTargetFPS(60);
			InitAudioDevice();
			currentScene = Scene::Menu;
			newCurrentScene = currentScene;
			gameOver = false;
			volumeFx = MASTER_VOLUME_INIT;
			volumeMusic = MASTER_VOLUME_INIT;
			resolucionCambio = false;
			menu::init();
		}

		static void update()
		{
			if (newCurrentScene != currentScene) //me movi de escena, por lo tanto tengo de destruir la anterior y cargar la nueva.
			{
				if (currentScene == game::Scene::Game)
				{
					menu::scenes::actual_menu_scenes_nueva = menu::scenes::menu_scenes::general;
				}
					
				switch (currentScene)
				{
				case Scene::Menu:
					menu::deinit();
					break;
				case Scene::Game:
					gameplay::deinit();
					break;
				}
				currentScene = newCurrentScene;
				switch (currentScene)
				{
				case Scene::Menu:
					menu::init();
					break;
				case Scene::Game:
					gameplay::init();
					break;
				}
			}
			switch (currentScene)
			{
			case Scene::Menu:
				if (menu::scenes::actual_menu_scenes != menu::scenes::actual_menu_scenes_nueva||resolucionCambio) //o cambio de res.
				{
					menu::deinit();
					menu::scenes::actual_menu_scenes = menu::scenes::actual_menu_scenes_nueva;
					resolucionCambio = false;
					menu::init();
				}
				menu::update();
				break;
			case Scene::Game:
				gameplay::update();
				break;
			}
		}

		static void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);

				switch (currentScene)
				{
				case Scene::Menu:
					if (!menu::cambioDeEsena)
					{
						menu::draw();
					}
					break;
				case Scene::Game:
					gameplay::draw();
					break;
				}

			EndDrawing();
		}

		static void deinit()
		{
			CloseWindow();
		}
	}
}