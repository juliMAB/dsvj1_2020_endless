#ifndef GAME_H
#define GAME_H

#include "raylib.h"

#define MASTER_VOLUME_INIT 1
#define MAX_RESOLUSION 6
namespace endless
{

	namespace game
	{

		enum class Scene
		{
			Menu,
			Game
		};

		extern Scene currentScene;
		extern Scene newCurrentScene;
		extern bool gameOver;
		extern float volumeFx;
		extern float volumeMusic;
		extern short resolucionSeleccionada;
		extern bool resolucionCambio;
		extern Vector2 resoluccion[MAX_RESOLUSION+1];
		void run();

		static void init();
		static void update();
		static void draw();
		static void deinit();
	}
}

#endif